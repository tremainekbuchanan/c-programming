/* Purpose : To generate and print a random playing card
// Date: June 3, 2012
// Author: Tremaine Buchanan
*/

#include <stdio.h>
#include <stdlib.h> 
#include <conio.h>

/*Function prototypes */
int face(void);
void card(int);
void printCard(char,int,int);


int main()
{
	int n = 0;

	n = face();
	
	card(n);

   getch();

	return 0;
}

int face()
{
	int randomNum = 0;

   	randomize();

	randomNum = rand()%13+1;
	return randomNum;
}

void card(int num)
{
	int suit = 0;

   char letter;

	//randomize();
   suit = rand()% 4 + 3;

    
	switch(num)
	{
		case 1:  letter = 'A'; printCard(letter,suit,0);
		break;

		case 11:letter = 'J'; printCard(letter,suit,0);
		break;
		
		case 12:letter = 'Q'; printCard(letter,suit,0);
		break;

		case 13:letter = 'K'; printCard(letter,suit,0);
		break;

      default:printCard(letter,suit,num);

		//default:printf("%d\%c", num, suit);
		break;
	}   
}

void printCard(char l, int s, int n)
{

	switch(n)
	{
		case 0: printf("______\n");
				printf("|   %c|\n", s);
				printf("|    |\n");
				printf("|%c   |\n", l);
				printf("|____|\n");
		break;
		
		default:printf("______\n");
				printf("|   %c|\n", s);
				printf("|    |\n");
				printf("|%d  |\n", n);
				printf("|____|\n");
		break;
}
}
